from rest_framework import serializers


class WindSerializer(serializers.Serializer):
    """Данные о скорости ветра"""
    speed = serializers.FloatField()


class MainSerializer(serializers.Serializer):
    """Данные об температуре, атмосферном давление, влажности воздуха"""
    temp = serializers.FloatField()
    pressure = serializers.IntegerField()
    humidity = serializers.IntegerField()


class WeatherSerializer(serializers.Serializer):
    """Данные о погодных условиях"""
    description = serializers.CharField(max_length=100)


class CurrentWeatherSerializer(serializers.Serializer):
    """Данные о текущей погоде"""
    weather = WeatherSerializer(many=True)
    main = MainSerializer()
    wind = WindSerializer()
    name = serializers.CharField(max_length=50, required=False)


class RegionsSerializer(serializers.Serializer):
    """Данные о регионе"""
    title = serializers.CharField(max_length=50)
    subtitle = serializers.CharField(max_length=100)
    country_code = serializers.CharField(max_length=50)
