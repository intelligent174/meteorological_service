from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import ParseError

from weather_forecast.exceptions import CurrentWeatherServiceError
from weather_forecast.exceptions import CurrentWeatherServiceIPAddressError
from weather_forecast.exceptions import YandexLocationServiceError
from weather_forecast.serializers import CurrentWeatherSerializer
from weather_forecast.serializers import RegionsSerializer
from weather_forecast.services import current_weather_service
from weather_forecast.services import get_client_ip
from weather_forecast.services import yandex_location_service


class RegionListView(APIView):
    """Вывод списка городов соответствующих запросу пользователя"""

    def get(self, request):
        city = request.query_params.get('city')
        country = request.query_params.get('country', '')

        try:
            regions = yandex_location_service.get_regions(city, country)
            serializer = RegionsSerializer(data=regions, many=True)

            if serializer.is_valid():
                return Response(serializer.data)
            else:
                return Response(status=400)

        except YandexLocationServiceError as e:
            raise ParseError from e


class CurrentWeatherView(APIView):
    """Вывод данных о текущей погоде"""

    def get(self, request):

        current_weather = None

        match request.query_params:
            case {
                'city': city,
                'country_code': country_code,
                'temperature_unit': temperature_unit
            } if city:
                try:
                    current_weather = current_weather_service.find_out_current_weather(
                        city=city,
                        country_code=country_code,
                        temperature_unit=temperature_unit
                    )

                except CurrentWeatherServiceError as e:
                    raise ParseError from e

            case {'temperature_unit': str(temperature_unit)}:
                client_ip = get_client_ip(request)

                try:
                    current_weather = current_weather_service.find_out_current_weather_by_ip_address(
                        client_ip, temperature_unit
                    )

                except CurrentWeatherServiceIPAddressError as e:
                    raise ParseError from e

        serializer = CurrentWeatherSerializer(data=current_weather)

        if serializer.is_valid():
            return Response(serializer.data)
        else:
            return Response(status=400)
