from dataclasses import dataclass


@dataclass(frozen=True)
class Region:
    country_code: str
    city: str
