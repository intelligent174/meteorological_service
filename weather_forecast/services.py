from api.current_weather import current_weather_api
from api.exceptions import CurrentWeatherAPIError
from api.exceptions import UserIPAddressAPIError
from api.exceptions import YandexLocationAPIError
from api.user_ip_address import user_ip_address_api
from api.yandex_location import YandexLocationApi
from weather_forecast.data_classes import Region
from weather_forecast.exceptions import CurrentWeatherServiceError
from weather_forecast.exceptions import CurrentWeatherServiceIPAddressError
from weather_forecast.exceptions import YandexLocationServiceError


def get_client_ip(request) -> str:
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

    if x_forwarded_for:
        client_ip = x_forwarded_for.split(',')[0]
    else:
        client_ip = request.META.get('REMOTE_ADDR')

    return client_ip


class CurrentWeatherService:

    def find_out_current_weather_by_ip_address(
            self,
            client_ip: str,
            temperature_unit: str
    ) -> dict:
        try:
            region = self._get_region_by_ip_address(client_ip)
            return self.find_out_current_weather(
                city=region.city,
                country_code=region.country_code,
                temperature_unit=temperature_unit
            )
        except CurrentWeatherServiceError as e:
            raise CurrentWeatherServiceIPAddressError from e

    @staticmethod
    def find_out_current_weather(
            city: str,
            country_code: str,
            temperature_unit: str
    ) -> dict:
        try:
            return current_weather_api.get_current_weather_data(
                city=city,
                country=country_code,
                temperature_unit=temperature_unit,
            )

        except CurrentWeatherAPIError as e:
            raise CurrentWeatherServiceError from e

    @classmethod
    def _get_region_by_ip_address(cls, ip_address: str) -> Region:
        try:
            return user_ip_address_api.get_region(ip_address)

        except UserIPAddressAPIError as e:
            raise CurrentWeatherServiceError from e


class YandexLocationService:
    def __init__(self, api: YandexLocationApi):
        self.api = api

    def get_regions(self, city: str, country: str) -> list[dict]:

        try:
            return self.api.get_regions(city, country)
        except YandexLocationAPIError as e:
            raise YandexLocationServiceError from e


yandex_location_service = YandexLocationService(YandexLocationApi())
current_weather_service = CurrentWeatherService()
