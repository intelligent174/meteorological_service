from exceptions import ClientError


class CurrentWeatherServiceError(ClientError):
    pass


class CurrentWeatherServiceIPAddressError(ClientError):
    pass


class YandexLocationServiceError(ClientError):
    pass
