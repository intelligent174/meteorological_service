from django.urls import path

from weather_forecast.views import CurrentWeatherView, RegionListView

urlpatterns = [
    path('current_weather/', CurrentWeatherView.as_view()),
    path('regions/', RegionListView.as_view())
]
