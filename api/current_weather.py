import requests

from api.exceptions import CurrentWeatherAPIError


class CurrentWeatherAPI:
    API_URL = f'https://api.openweathermap.org/data/2.5/weather'
    API_KEYS = f'b87f53037ca5bcb232828eb1f2a6d699'

    def get_current_weather_data(
            self,
            city: str,
            country: str,
            temperature_unit: str,
            language: str = 'ru'
    ) -> dict:
        try:
            params = {
                'q': f'{city},{country}',
                'appid': self.API_KEYS,
                'lang': language,
                'units': temperature_unit,
            }
            response = requests.get(self.API_URL, params=params)
            response.raise_for_status()
        except requests.exceptions.RequestException as e:
            raise CurrentWeatherAPIError from e
        return response.json()


current_weather_api = CurrentWeatherAPI()
