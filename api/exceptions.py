from exceptions import ClientError


class CurrentWeatherAPIError(ClientError):
    pass


class YandexLocationAPIError(ClientError):
    pass


class UserIPAddressAPIError(ClientError):
    pass
