import requests

from api.exceptions import UserIPAddressAPIError
from weather_forecast.data_classes import Region


class UserIPAddressAPI:
    ID_OF_FIELDS = 57362
    URL_IP_API = f'http://ip-api.com/json/'

    def get_region(self, client_ip: str) -> Region:
        country_code, city = self._send_request(self.URL_IP_API, client_ip)

        return self._create_object(city, country_code)

    @classmethod
    def _send_request(cls, url: str, client_ip: str) -> tuple:
        params = {'fields': cls.ID_OF_FIELDS}
        response = requests.get(f'{url}{client_ip}', params=params)

        match response.json():
            case {'countryCode': str(country_code), 'city': str(city)}:
                return country_code, city
            case {'message': message} if message:
                raise UserIPAddressAPIError(message)

    @classmethod
    def _create_object(cls, city: str, country_code: str) -> Region:
        return Region(
            country_code=country_code,
            city=city,
        )


user_ip_address_api = UserIPAddressAPI()
