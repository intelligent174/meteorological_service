import pycountry
import requests

from deep_translator import PonsTranslator
from django.contrib.gis.geoip2.resources import Country

from api.exceptions import YandexLocationAPIError


class YandexLocationApi:
    URL_YANDEX_API = f'http://suggest-maps.yandex.ru/suggest-geo'

    def get_regions(self, city: str, country: str) -> list[dict]:

        data_by_regions = self._send_request(
            url=self.URL_YANDEX_API,
            city=city,
            country=country,
        )

        return [self._make_region(region) for region in data_by_regions]

    @classmethod
    def _make_country_code(cls, country: str) -> Country:
        return pycountry.countries.search_fuzzy(country)[0]

    @classmethod
    def _make_region(cls, region: dict) -> dict:
        country = region.get('subtitle', '').rsplit(', ', 1)[-1]
        translated_country_name = cls._translate(country)
        country_code = cls._make_country_code(translated_country_name)

        return {
            'title': region.get('title', ''),
            'subtitle': region.get('subtitle', ''),
            'country_code': country_code.alpha_2,
        }

    @classmethod
    def _send_request(
            cls,
            url: str,
            city: str,
            country: str,
            max_results: int = 20
    ) -> dict:

        params = {
            'search_type': 'tune',
            'v': '9',
            'results': max_results,
            'lang': 'ru',
            'part': f'{country} {city}'.strip()
        }
        response = requests.get(url, params=params)

        match response.json():
            case {'results': list(results)} if results:
                return results
            case _:
                raise YandexLocationAPIError('Entered incorrect data')

    @classmethod
    def _translate(
            cls,
            text: str,
            source_language: str = 'ru',
            target_language: str = 'en'
    ) -> str:
        return PonsTranslator(source=source_language, target=target_language).translate(text)
